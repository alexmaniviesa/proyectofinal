<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>Blog de Preguntas Java</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	       <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>   
	       </ul>
	       
	    </nav>
	    <h3 class="text-muted">Blog Preguntas</h3>
    
    </div>
    
 	    <div class="jumbotron">
          <h1>El blog sobre Preguntas</h1>
          <p>Preguntas Java.</p>
          <c:if test="${empty sessionScope.userLoggedIn }">
               <p><a href="/signup"  role="button"  class="btn btn-lg btn-success ">Regístrate</a></p>
		  </c:if>
        </div>   

		
		<c:forEach items="${listaPost}" var="postItem">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<h1><a href="/post/${postItem.id}">${postItem.titulo}</a></h1>
				<div>
					<div class="pull-right" style="padding: 10px 0 0 5px;">${postItem.autor.nombre}</div>
					<img alt="User Pic" src="http://i.pravatar.cc/50?u=${postItem.autor.email}"
						class="img-circle img-responsive pull-right">
					<p></p>
				</div>
				<div style="clear: both; margin-bottom: 10px;"></div>
					<c:if test="${not empty postItem.url}">
						<p><a href="${postItem.url}">${postItem.url}</a></p>
					</c:if>
					<div class="dotdotdot">
						<p>${postItem.contenido}</p>				
					</div>
				<div>
					<span class="badge">Escrito el <fmt:formatDate pattern="dd/MM/yyyy" value="${postItem.fecha}" /> a las 
					<fmt:formatDate pattern="HH:mm:ss" value="${postItem.fecha}" /></span>
					<span class="label label-info">					
                        ${(fn:length(postItem.comments) gt 0) ? fn:length(postItem.comments) : 'Sin '} 
						${(fn:length(postItem.comments) == 1) ? 'comentario' : 'comentarios'} 					
						</span>
				</div>
				<hr>
			</div>
		</div>
		</c:forEach>

        
  
  
  </div>
  
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
   <script src="assets/js/jquery.dotdotdot.min.js"> </script>
    <script>
 jQuery(document).ready(function() {

   $(".dotdotdot").dotdotdot({
      height:150
   });

}); 

 </script>

  
</body>
</html>